package player.lomotif.com.lomotifplayer.utils

import android.arch.lifecycle.MutableLiveData
import com.google.android.exoplayer2.ExoPlaybackException
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.mediacodec.MediaCodecRenderer
import com.google.android.exoplayer2.source.BehindLiveWindowException

class PlayerEventListener : Player.DefaultEventListener() {

    val updateResumePosition: MutableLiveData<Boolean> by lazy { MutableLiveData<Boolean>() }

    val inErrorState: MutableLiveData<Boolean> by lazy { MutableLiveData<Boolean>() }

    val behindLiveWindow: MutableLiveData<Boolean> by lazy { MutableLiveData<Boolean>() }

    val isLoadingVideo: MutableLiveData<Boolean> by lazy { MutableLiveData<Boolean>() }

    val openVideoPlayer: MutableLiveData<Boolean> by lazy { MutableLiveData<Boolean>() }

    private var hasPlayerError: Boolean = false

    override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
        //apply loading
        isLoadingVideo.postValue(playbackState == Player.STATE_BUFFERING || inErrorState.value ?: false)
    }

    override fun onPositionDiscontinuity(@Player.DiscontinuityReason reason: Int) {
        if (hasPlayerError) {
            // This will only occur if the user has performed a seek whilst in the error state. Update
            // the resume position so that if the user then retries, playback will resume from the
            // position to which they seeked.
            updateResumePosition.postValue(true)

        }
    }

    override fun onPlayerError(e: ExoPlaybackException?) {
        var hasErrorInRendering = false

        if (e?.type == ExoPlaybackException.TYPE_RENDERER) {
            val cause = e.rendererException
            if (cause is MediaCodecRenderer.DecoderInitializationException) {
                // Special case for decoder initialization failures.
                cause.decoderName?.let {
                    hasErrorInRendering = true
                    openVideoPlayer.postValue(true)
                }
            }
        }

        hasPlayerError = true

        inErrorState.postValue(!hasErrorInRendering)

        behindLiveWindow.postValue(isBehindLiveWindow(e))
    }

    private fun isBehindLiveWindow(e: ExoPlaybackException?): Boolean {
        e?.let {
            if (it.type != ExoPlaybackException.TYPE_SOURCE) {
                return false
            }
            var cause: Throwable? = it.sourceException
            while (cause != null) {
                if (cause is BehindLiveWindowException) {
                    return true
                }
                cause = cause.cause
            }
        }
        return false
    }
}