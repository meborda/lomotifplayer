package player.lomotif.com.lomotifplayer.extensions

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import android.text.TextUtils
import android.widget.Toast
import player.lomotif.com.lomotifplayer.R
import java.util.concurrent.TimeUnit

fun Long.toFormattedTime(): String {
    val format: String = if (this >= 60000) "%d:%02d" else "%02d:%02d"

    return String.format(
        format, TimeUnit.MILLISECONDS.toMinutes(this) % 60,
        TimeUnit.MILLISECONDS.toSeconds(this) % 60
    )
}

fun Context.openAppSettings() {
    val intent = Intent()
    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
    val uri = Uri.fromParts("package", packageName, null)
    intent.data = uri
    startActivity(intent)
}

fun Context.dispatchVideoPlayIntent(link: String) {
    try {
        if (!TextUtils.isEmpty(link)) {
            val path = Uri.parse(link)
            val intent = Intent(Intent.ACTION_VIEW, path)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.setDataAndType(path, "video/*")
            val chooser = Intent.createChooser(intent, getString(R.string.select_video_app))
            chooser.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(chooser)
        }
    } catch (exception: ActivityNotFoundException) {
        Toast.makeText(this, R.string.play_video_app, Toast.LENGTH_SHORT).show()
    }

}