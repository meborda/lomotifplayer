package player.lomotif.com.lomotifplayer.models

import android.os.Parcel
import android.os.Parcelable

data class MediaFile(
    val path: String = "",
    val title: String = "",
    val duration: Long = -1,
    var type: Int? = null
) : Parcelable {

    override fun equals(other: Any?): Boolean {
        val otherMedia = other as MediaFile
        return path == otherMedia.path
    }

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readLong(),
        parcel.readInt()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(path)
        parcel.writeString(title)
        parcel.writeLong(duration)
        parcel.writeInt(type ?: -1)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MediaFile?> {

        const val MEDIA_TYPE_VIDEO = "video/"
        const val MEDIA_TYPE_AUDIO = "audio/"

        override fun createFromParcel(parcel: Parcel): MediaFile? {
            return MediaFile(parcel)
        }

        override fun newArray(size: Int): Array<MediaFile?> {
            return arrayOfNulls(size)
        }
    }

}