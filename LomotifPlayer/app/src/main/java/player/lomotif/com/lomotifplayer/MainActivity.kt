package player.lomotif.com.lomotifplayer

import android.Manifest
import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import player.lomotif.com.lomotifplayer.adapters.MediaGalleryAdapter
import player.lomotif.com.lomotifplayer.extensions.openAppSettings
import player.lomotif.com.lomotifplayer.models.MediaFile
import player.lomotif.com.lomotifplayer.utils.PermissionHandler
import player.lomotif.com.lomotifplayer.viewmodels.MainViewModel


class MainActivity : AppCompatActivity(), PermissionHandler.PermissionResultCallback {

    private lateinit var viewModel: MainViewModel
    private lateinit var adapter: MediaGalleryAdapter
    private var playerDialog: PlayerDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        adapter = MediaGalleryAdapter(this, mediaOnClick)
        recycler.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recycler.adapter = adapter
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        viewModel.mediaFiles.observe(this, Observer<MutableList<MediaFile>> {
            it?.let { files ->
                adapter.files.replaceAll(files)
            }
        })
    }

    override fun onStart() {
        super.onStart()
        retrieveFiles()
    }

    fun retrieveFiles() {
        if (viewModel.isPermissionGranted())
            viewModel.retrieveFiles()
        else {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                PERMISSION_READ_REQUEST
            )
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == PERMISSION_READ_REQUEST)
            PermissionHandler.handlePermissionResult(this, permissions, grantResults, this)
    }

    override fun onPermissionGranted() {
        viewModel.retrieveFiles()
    }

    override fun onPermissionDenied() {
        Toast.makeText(this, R.string.permission_denied, Toast.LENGTH_SHORT).show()
    }

    override fun onDeniedNeverAskTriggered() {
        showPermissionSnackbar()
    }

    private fun showPermissionSnackbar() {
        if (lifecycle.currentState.isAtLeast(Lifecycle.State.CREATED)) {
            val permissionSnackbar = Snackbar.make(
                container,
                R.string.permission_denied,
                Snackbar.LENGTH_INDEFINITE
            )
            permissionSnackbar.setAction(R.string.permission_grant) {
                openAppSettings()
            }
            permissionSnackbar.show()
        }
    }

    val mediaOnClick = View.OnClickListener {
        it?.let {
            var tag = it.tag
            if (tag != null && tag is MediaFile && !isFinishing &&
                (playerDialog == null || (playerDialog?.isAdded == false
                        && playerDialog?.isResumed == false))
            ) {
                playerDialog = PlayerDialog.newInstance(tag)
                playerDialog?.show(supportFragmentManager, "MediaPlayer")
            }
        }
    }

    companion object {
        const val PERMISSION_READ_REQUEST = 1001
    }

}
