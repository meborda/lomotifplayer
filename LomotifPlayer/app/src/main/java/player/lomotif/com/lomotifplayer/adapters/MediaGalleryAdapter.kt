package player.lomotif.com.lomotifplayer.adapters

import android.content.Context
import android.provider.MediaStore
import android.support.v7.util.SortedList
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_media.view.*
import player.lomotif.com.lomotifplayer.R
import player.lomotif.com.lomotifplayer.extensions.toFormattedTime
import player.lomotif.com.lomotifplayer.models.MediaFile

class MediaGalleryAdapter(
    private val context: Context,
    val onClick: View.OnClickListener
) :
    RecyclerView.Adapter<MediaGalleryAdapter.MediaFileViewHolder>() {

    override fun onBindViewHolder(holder: MediaFileViewHolder, position: Int) {
        val file = files[position]
        when (file.type) {
            MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO -> {
                Glide.with(context)
                    .load(file.path)
                    .apply(RequestOptions().centerCrop().frame(1_000_000))
                    .into(holder.imageThumbnail)
                holder.imagePlay.visibility = View.VISIBLE
            }
            MediaStore.Files.FileColumns.MEDIA_TYPE_AUDIO -> {
                Glide.with(context)
                    .load(android.R.drawable.ic_btn_speak_now)
                    .into(holder.imageThumbnail)
                holder.imagePlay.visibility = View.GONE
            }
        }
        holder.durationText.text = file.duration.toFormattedTime()
        holder.titleText.text = file.title
        holder.itemView.tag = file
        holder.itemView.setOnClickListener(onClick)
    }

    override fun getItemCount(): Int {
        return files.size()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MediaFileViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_media, parent, false)
        return MediaFileViewHolder(view)
    }

    class MediaFileViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val imageThumbnail: ImageView = itemView.imageThumbnail
        val imagePlay: ImageView = itemView.imagePlay
        val durationText: TextView = itemView.durationText
        val titleText: TextView = itemView.titleText
    }

    var files: SortedList<MediaFile> = SortedList(MediaFile::class.java,
        object : SortedList.Callback<MediaFile>() {
            override fun areItemsTheSame(item1: MediaFile, item2: MediaFile): Boolean {
                return item1 == item2
            }

            override fun onMoved(fromPosition: Int, toPosition: Int) {
                notifyItemMoved(fromPosition, toPosition)
            }

            override fun onChanged(position: Int, count: Int) {
                notifyItemRangeChanged(position, count)
            }

            override fun onInserted(position: Int, count: Int) {
                notifyItemRangeInserted(position, count)
            }

            override fun onRemoved(position: Int, count: Int) {
                notifyItemRangeRemoved(position, count)
            }

            override fun compare(oldItem: MediaFile, newItem: MediaFile): Int {
                return 0
            }

            override fun areContentsTheSame(item1: MediaFile, item2: MediaFile): Boolean {
                return item1 == item2
            }
        })

}
