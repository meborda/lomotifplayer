package player.lomotif.com.lomotifplayer.viewmodels

import android.Manifest
import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.content.pm.PackageManager
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import player.lomotif.com.lomotifplayer.models.MediaFile
import player.lomotif.com.lomotifplayer.models.MediaFile.CREATOR.MEDIA_TYPE_VIDEO

class MainViewModel(val app: Application) : AndroidViewModel(app) {

    val mediaFiles: MutableLiveData<MutableList<MediaFile>> = MutableLiveData()

    init {
        mediaFiles.value = mutableListOf()
    }

    fun isPermissionGranted(): Boolean {
        return ActivityCompat.checkSelfPermission(
            app,
            Manifest.permission.READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED
    }

    fun retrieveFiles() {
        val cursor = app.contentResolver.query(
            MediaStore.Files.getContentUri("external"),
            arrayOf(
                MediaStore.Video.Media.DATA, MediaStore.Video.Media.TITLE,
                MediaStore.Video.Media.DURATION, MediaStore.Video.Media.MIME_TYPE
            ),
            MediaStore.Files.FileColumns.MEDIA_TYPE + "=" +
                    MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO + " OR " +
                    MediaStore.Files.FileColumns.MEDIA_TYPE + "=" +
                    MediaStore.Files.FileColumns.MEDIA_TYPE_AUDIO,
            null,
            MediaStore.Files.FileColumns.DATE_ADDED + " DESC"
        )
        cursor?.let {
            if (it.moveToFirst()) {
                val pathIdx = it.getColumnIndex(MediaStore.Video.Media.DATA)
                val titleIdx = it.getColumnIndex(MediaStore.Video.Media.TITLE)
                val durationIdx = it.getColumnIndex(MediaStore.Video.Media.DURATION)
                val typeIdx = it.getColumnIndex(MediaStore.Video.Media.MIME_TYPE)
                do {
                    val media = MediaFile(
                        it.getString(pathIdx),
                        it.getString(titleIdx),
                        it.getLong(durationIdx)
                    )
                    val type =
                        if (it.getString(typeIdx).toLowerCase().startsWith(MEDIA_TYPE_VIDEO))
                            MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO
                        else MediaStore.Files.FileColumns.MEDIA_TYPE_AUDIO
                    media.type = type
                    mediaFiles.value?.add(media)
                } while (it.moveToNext())
            }
        }
        mediaFiles.postValue(mediaFiles.value)
    }

}