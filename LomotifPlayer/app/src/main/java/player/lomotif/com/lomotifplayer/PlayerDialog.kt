package player.lomotif.com.lomotifplayer

import android.arch.lifecycle.Observer
import android.content.Context
import android.media.AudioAttributes
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import kotlinx.android.synthetic.main.dialog_player.*
import player.lomotif.com.lomotifplayer.extensions.dispatchVideoPlayIntent
import player.lomotif.com.lomotifplayer.models.MediaFile
import player.lomotif.com.lomotifplayer.utils.AudioFocus
import player.lomotif.com.lomotifplayer.utils.PlayerEventListener

class PlayerDialog : DialogFragment() {

    private var player: SimpleExoPlayer? = null
    private var audioFocus: AudioFocus? = null
    private var trackSelector: DefaultTrackSelector? = null
    private val playerEventListener = PlayerEventListener()
    private val bandwidthMeter = DefaultBandwidthMeter()

    private var resumeWindow: Int = 0
    private var resumePosition: Long = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_player, container, false)
    }


    private fun releasePlayer() {
        player?.let {
            resumePosition = Math.max(0, player?.currentPosition ?: 0L)
            resumeWindow = player?.currentWindowIndex ?: 0

            player?.playWhenReady = false
            player?.release()
            player = null
        }

        audioFocus?.pausePlayback()
    }

    private fun clearResumePosition() {
        resumeWindow = C.INDEX_UNSET
        resumePosition = C.TIME_UNSET
    }

    private fun updateResumePosition() {
        player?.let {
            resumeWindow = it.currentWindowIndex
            resumePosition = Math.max(0, it.contentPosition)
        }
    }

    private fun initializePlayer(mediaUrl: String) {
        //need a new Player
        if (player == null) {
            val adaptiveTrackSelectionFactory = AdaptiveTrackSelection.Factory(bandwidthMeter)
            trackSelector = DefaultTrackSelector(adaptiveTrackSelectionFactory)

            //create player
            player = ExoPlayerFactory.newSimpleInstance(
                DefaultRenderersFactory(context),
                trackSelector,
                DefaultLoadControl()
            )
            exoPlayer.player = player

            //add playerEvent Listener to check if player has error in decoding.
            player?.addListener(playerEventListener)
        }

        //check if player has already play
        val haveResumePosition = resumeWindow != C.INDEX_UNSET
        if (haveResumePosition) {
            player?.seekTo(resumeWindow, resumePosition)
        }

        val mediaUri = Uri.parse(mediaUrl)
        val mediaSource = buildMediaSource(mediaUri)

        player?.prepare(mediaSource, !haveResumePosition, false)
        player?.playWhenReady = true

        audioFocus?.requestAudioFocus()
    }

    // Produces DataSource instances through which media data is loaded.
    private fun buildMediaSource(mediaUri: Uri): ExtractorMediaSource? {
        val bandwidthMeter = DefaultBandwidthMeter()
        val dataSourceFactory = DefaultDataSourceFactory(
            context?.applicationContext,
            Util.getUserAgent(
                context?.applicationContext,
                context?.applicationInfo?.loadLabel(context?.packageManager).toString()
            ),
            bandwidthMeter
        )

        return ExtractorMediaSource
            .Factory(dataSourceFactory)
            .createMediaSource(mediaUri)
    }

    fun attachListener(videoUrl: String) {
        playerEventListener.updateResumePosition.observe(this, Observer {
            updateResumePosition()
        })

        playerEventListener.inErrorState.observe(this, Observer {
            it?.let {
                Toast.makeText(context, R.string.player_error, Toast.LENGTH_SHORT).show()
            }
        })

        playerEventListener.behindLiveWindow.observe(this, Observer {
            it?.let {
                if (it) {
                    clearResumePosition()
                    initializePlayer(videoUrl)
                } else {
                    updateResumePosition()
                }
            }
        })

        playerEventListener.isLoadingVideo.observe(this, Observer {
            it?.let { loadingMediaProgress.visibility = if (it) View.VISIBLE else View.GONE }
        })

        playerEventListener.openVideoPlayer.observe(this, Observer {
            it?.let {
                // in case device doesn't support exoplayer
                context?.dispatchVideoPlayIntent(videoUrl)
                Toast.makeText(context, R.string.player_error, Toast.LENGTH_SHORT).show()
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val mediaFile = arguments?.getParcelable<MediaFile>(EXTRA_MEDIA_FILE)
        mediaFile?.let {
            mediaTitle.text = mediaFile.title
            initializePlayer(mediaFile.path)
            attachListener(mediaFile.path)
        } ?: run {
            dismiss()
        }
    }

    override fun onStart() {
        super.onStart()
        adjustDialogSize()
    }

    override fun onPause() {
        super.onPause()
        if (Util.SDK_INT <= Build.VERSION_CODES.M) {
            releasePlayer()
        }
    }

    override fun onStop() {
        super.onStop()
        if (Util.SDK_INT > Build.VERSION_CODES.M) {
            releasePlayer()
        }
    }

    private fun adjustDialogSize() {
        val layoutParams = dialog.window?.attributes
        val displayMetrics = DisplayMetrics()
        activity?.windowManager?.defaultDisplay?.getMetrics(displayMetrics)
        layoutParams?.width = (displayMetrics.widthPixels * 0.90).toInt()
        dialog.window.attributes = layoutParams
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        context?.let {
            audioFocus = AudioFocus(
                it,
                AudioAttributes.USAGE_MEDIA,
                AudioAttributes.CONTENT_TYPE_MOVIE
            )
        }
    }

    companion object {
        const val EXTRA_MEDIA_FILE = "extra_media_file"

        fun newInstance(mediaFile: MediaFile): PlayerDialog {
            val fragment = PlayerDialog()
            val bundle = Bundle()
            bundle.putParcelable(EXTRA_MEDIA_FILE, mediaFile)
            fragment.arguments = bundle
            return fragment
        }
    }

}