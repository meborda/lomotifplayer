package player.lomotif.com.lomotifplayer.utils

import android.app.Activity
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat

object PermissionHandler {

    interface PermissionResultCallback {
        fun onPermissionGranted()

        fun onPermissionDenied()

        fun onDeniedNeverAskTriggered()
    }

    fun handlePermissionResult(
        activity: Activity, permissions: Array<out String>,
        grantResults: IntArray,
        callback: PermissionResultCallback
    ) {

        var allGranted = true
        var neverAsk = true

        if (grantResults.isNotEmpty()) {
            grantResults.forEach {
                if (it != PackageManager.PERMISSION_GRANTED) {
                    allGranted = false
                }
            }

            if (allGranted) {
                callback.onPermissionGranted()
            } else {

                permissions.forEach {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(activity, it)) {
                        neverAsk = false
                    }
                }
                if (neverAsk) {
                    // Permission never ask again is triggered
                    callback.onDeniedNeverAskTriggered()
                } else {
                    callback.onPermissionDenied()
                }
            }
        }
    }

}