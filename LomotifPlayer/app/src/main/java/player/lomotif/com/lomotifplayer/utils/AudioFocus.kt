package player.lomotif.com.lomotifplayer.utils

import android.content.Context
import android.media.AudioAttributes
import android.media.AudioFocusRequest
import android.media.AudioManager
import android.os.Build
import android.util.Log

class AudioFocus(
    val context: Context,
    usageAttributes: Int,
    contentTypeAttributes: Int
) : AudioManager.OnAudioFocusChangeListener {

    private val TAG = AudioFocus::class.java.simpleName
    private var audioManager: AudioManager? = null
    private var playBackAttributes: AudioAttributes? = null
    private var focusRequest: AudioFocusRequest? = null

    init {
        audioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager

        playBackAttributes = AudioAttributes.Builder()
            .setUsage(usageAttributes)
            .setContentType(contentTypeAttributes)
            .build()

        initFocusRequest()

    }

    private fun initFocusRequest() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            focusRequest = AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN_TRANSIENT)
                .setAudioAttributes(playBackAttributes)
                .setAcceptsDelayedFocusGain(true)
                .setOnAudioFocusChangeListener(this)
                .build()
        }
    }

    override fun onAudioFocusChange(focusChange: Int) {
        when (focusChange) {
            AudioManager.AUDIOFOCUS_GAIN -> {
                Log.d(TAG, "AUDIO_FOCUS_GAIN")
            }
            AudioManager.AUDIOFOCUS_LOSS -> {
                Log.d(TAG, "AUDIO_FOCUS_LOSS")
                pausePlayback()
            }
            AudioManager.AUDIOFOCUS_LOSS_TRANSIENT -> {
                Log.d(TAG, "AUDIO_FOCUS_LOSS_TRANSIENT")
                pausePlayback()
            }
        }
    }

    fun requestAudioFocus(): Int? {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            audioManager?.requestAudioFocus(focusRequest)
        } else {
            audioManager?.requestAudioFocus(
                this,
                AudioManager.STREAM_MUSIC,
                AudioManager.AUDIOFOCUS_GAIN_TRANSIENT
            )
        }
    }

    fun pausePlayback() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            audioManager?.abandonAudioFocusRequest(focusRequest)
        } else {
            audioManager?.abandonAudioFocus(this)
        }
    }

}